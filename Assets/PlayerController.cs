﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    Normal,
    SpeedUp
}

public class PlayerController : MonoBehaviour {

    public static PlayerController instance;
    private bool _canMove = false;
    private Rigidbody2D rigi2D;
    private int _speed = 7;

    public bool CanMove
    {
        get { return _canMove; }
    }

    public float VelX
    {
        get { return rigi2D.velocity.x; }
    }

    private void Awake()
    {
        instance = this;
    }
    
    void Start () {
        rigi2D = transform.GetComponent<Rigidbody2D>();
	}
	
	void Update () {
        
            rigi2D.velocity = ScrollCircle.instance.dir*_speed *GameManager.instance.Speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "AirWall")
        {
            _canMove = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.transform.tag == "AirWall")
        {
            _canMove = false;
        }
    }
}
