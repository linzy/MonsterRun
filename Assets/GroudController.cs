﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitType{
   None,       // 空挡状态
   Start,      // 起始部件
   Process,    // 过程部件
   Both,       // 通用部件
   End         // 结束部件
}

public class GroudController : MonoBehaviour
{
    public Transform[] groundArr;
    // 背景物件
    private List<GameObject> unitList = new List<GameObject>();
    // 背景物件起始位置
    private Vector3 unitStartPos;
    // 距离
    private float distance;

    // 随机等待距离
    private float randomWaitDis;
    // 刷出部件时起始点离最后一个刷出物件的距离
    private float lastUnitDis;
    // 当前刷出部件的类型
    private UnitType curUnitType = UnitType.None;

    private void Awake()
    {
        unitStartPos = GameObject.Find("UnitStartPos").transform.position;
    }

    void Start()
    {

    }

    void Update()
    {

        CreateBgUnit();

        RunBg();

        RunUnit();

        CalculateDis();
    }

    /// <summary>
    /// 计算距离
    /// </summary>
    private void CalculateDis()
    {
        if (PlayerController.instance.CanMove)
            distance += ScrollCircle.instance.dir.x;

        GameManager.instance.UpdateDis(distance);
    }

    private bool CanCreateUnit()
    {
        if (unitList.Count < 1)
            return true;

        if (unitStartPos.x - unitList[unitList.Count -1].transform.position.x 
            >= unitList[unitList.Count - 1].GetComponent<BoxCollider2D>().size.x)
            return true;
        else
            return false;
    }

    /// <summary>
    /// 生成背景物件
    /// </summary>
    private void CreateBgUnit()
    {
        if (!CanCreateUnit())
            return;

        float random;
        
        switch (curUnitType)
        {
            case UnitType.None:
                // 可刷 start both
                random = Random.Range(0, 2);
                if (random > 0.5f)
                    CreateUnitOfTypeS();
                else
                    CreateUnitOfTypeB();
                break;
            case UnitType.Start:
                // 可刷 end both process
                random = Random.Range(0, 3);
                if (random > 2.5f)
                    CreateUnitOfTypeE();
                else if (random > 1.25f)
                    CreateUnitOfTypeB();
                else
                    CreateUnitOfTypeP();
                break;
            case UnitType.End:
                if (distance - lastUnitDis > randomWaitDis)
                curUnitType = UnitType.None;
                break;
            case UnitType.Process:
                // end both
                random = Random.Range(0, 2);
                if (random > 0.5f)
                    CreateUnitOfTypeE();
                else
                    CreateUnitOfTypeB();
                break;
            case UnitType.Both:
                // process end
                random = Random.Range(0, 3);
                if (random > 2)
                    CreateUnitOfTypeE();
                else if (random > 1)
                    CreateWait();
                else
                    CreateUnitOfTypeP();
                break;
            default:
                break;
        }
    }

    /// <summary>
    ///  滚动背景物件
    /// </summary>
    private void RunUnit()
    {
        if (!PlayerController.instance.CanMove)
            return;

        for(int i = 0; i < unitList.Count; i++)
        {
                if (unitList[i].transform.position.x < -18.5f)
                {
                    Destroy(unitList[i]);
                    unitList.Remove(unitList[i]);
                }
                else

                    unitList[i].transform.position += GameManager.instance.MovePos();
        }
    }

    /// <summary>
    /// 滚动背景
    /// </summary>
    private void RunBg()
    {
        // 滚动背景地图
        if (PlayerController.instance.CanMove)
        {
            for (int i = 0; i < groundArr.Length; i++)
            {
                groundArr[i].position += GameManager.instance.MovePos();
            }
        }

        if (groundArr[0].position.x < -groundArr[0].GetComponent<SpriteRenderer>().size.x * groundArr[0].localScale.x)
        {
            // 将滚出左边视界的地图重新排序到最后
            groundArr[0].position =
                new Vector3(groundArr[groundArr.Length - 1].position.x +
                groundArr[0].GetComponent<SpriteRenderer>().size.x * groundArr[0].localScale.x,
                groundArr[groundArr.Length - 1].position.y,
                groundArr[groundArr.Length - 1].position.z);
            // 重新排序数组
            Transform tempGround = groundArr[0];

            for (int i = 0; i < groundArr.Length - 1; i++)
            {
                groundArr[i] = groundArr[i + 1];
            }
            groundArr[groundArr.Length - 1] = tempGround;

            // 清除ground[0]

            // ground[length-1] 生成怪物、道具
        }
    }

    private void CreateUnitOfTypeS()
    {
        int typeNum = Random.Range(0, 1);
        GameObject bottle = Instantiate(Resources.Load("Prefabs/s" + typeNum.ToString()), new Vector3(unitStartPos.x, unitStartPos.y, -0.5f), Quaternion.identity) as GameObject;
        unitList.Add(bottle);
        curUnitType = UnitType.Start;
    }

    private void CreateUnitOfTypeB()
    {
        int typeNum = Random.Range(0, 4);
        GameObject bottle = Instantiate(Resources.Load("Prefabs/b" + typeNum.ToString()), new Vector3(unitStartPos.x, unitStartPos.y, 1f), Quaternion.identity) as GameObject;
        unitList.Add(bottle);
        curUnitType = UnitType.Both;
    }

    private void CreateUnitOfTypeE()
    {
        int typeNum = Random.Range(0, 1);
        GameObject bottle = Instantiate(Resources.Load("Prefabs/e" + typeNum.ToString()), new Vector3(unitStartPos.x, unitStartPos.y, -0.5f), Quaternion.identity) as GameObject;
        unitList.Add(bottle);

        CreateWait();
    }

    private void CreateWait()
    {
        // 设置随机等待距离
        randomWaitDis = Random.Range(20f, 80f);
        // 记录当前移动距离
        lastUnitDis = distance;

        curUnitType = UnitType.End;
    }

    private void CreateUnitOfTypeP()
    {
        int typeNum = Random.Range(0, 7);
        GameObject bottle = Instantiate(Resources.Load("Prefabs/p" + typeNum.ToString()), 
            new Vector3(unitStartPos.x,unitStartPos.y,-0.5f), Quaternion.identity) as GameObject;
        unitList.Add(bottle);
        curUnitType = UnitType.Process;
    }

}
