﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MainView : MonoBehaviour {

    public static MainView instance;
    public Text DistanceText;

    private void Awake()
    {
        instance = this;
    }

    void Start () {
        DistanceText = GameObject.Find("Canvas/DistanceText").GetComponent<Text>();
	}

    public void UpdateDis(float dis)
    {
        DistanceText.text = Math.Round(dis*0.1f,2).ToString()+"m";
    }
	
}
