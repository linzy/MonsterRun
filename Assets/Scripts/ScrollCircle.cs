﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollCircle : ScrollRect,IEndDragHandler{

    public static ScrollCircle instance;
    private bool isStop = false;
    protected float _radius = 0;
    public Vector2 dir;
    private Vector2 contentPos;
    
    
    protected override void Awake()
    {
        instance = this;
    }

    protected override void Start () {
        base.Start();
        // 计算半径
        _radius = (transform as RectTransform).sizeDelta.x * 0.5f;
	}

    public override void OnDrag(PointerEventData eventData)
    {
        base.OnDrag(eventData);
        // 获得相对于锚点的位置
        contentPos = content.anchoredPosition;

        // 获得摇杆偏离锚点位置的向量长度 限制摇杆区域在圆内
        if(contentPos.magnitude > _radius)
        {
            contentPos = contentPos.normalized * _radius;

            // 设置摇杆相对于锚点的位置
            SetContentAnchoredPosition(contentPos);
        }

        // 获得归一向量
        dir = (content.position - transform.position).normalized * contentPos.magnitude/_radius;
        
        isStop = false;
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);

        isStop = true;
    }

    protected void Update()
    {
        if (isStop)
        {
            dir.x = Mathf.Lerp(dir.x, 0, Time.deltaTime*5f);
            dir.y = Mathf.Lerp(dir.y, 0, Time.deltaTime*5f);
        }
    }
}
