﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 加速道具
/// </summary>
public class SpeedItem : MonoBehaviour {
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            GameManager.instance.SpeedUp(1f, 0.5f);
            ItemManager.instance.DestoryItem(this.gameObject);
        }
    }
}
