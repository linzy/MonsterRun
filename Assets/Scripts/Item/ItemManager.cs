﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour {

    public static ItemManager instance;

    List<GameObject> itemList = new List<GameObject>();

    // 上一次刷出道具的距离
    private int lastCreateDis;
    // 同屏刷出道具的上限数
    private const int maxItem = 6;
    // 刷出道具的随机距离间隔
    private int randomDis = 0;

    private void Awake()
    {
        instance = this;
    }
    
    void Start () {
		
	}
	
	void Update () {

        RunItem();

        CreateItem();

    }

    private void RunItem()
    {
        if (!PlayerController.instance.CanMove || itemList.Count < 1)
            return;

        for (int i = 0; i < itemList.Count; i++)
        {
            if(itemList[i] != null)
            {

            itemList[i].transform.position += GameManager.instance.MovePos();

            if (itemList[i].transform.position.x < -18.5f)
            {
                    this.DestoryItem(itemList[i]);
            }
            }
        }
    }

    private void CreateItem()
    {
        if (itemList.Count >= maxItem)
            return;

        if (GameManager.instance.GetDistance() - lastCreateDis < randomDis)
            return;

        lastCreateDis = (int)GameManager.instance.GetDistance();

        int typeNum = Random.Range(0, 1);
        GameObject bottle = Instantiate(Resources.Load("Prefabs/i" + typeNum.ToString()), new Vector3(13f, Random.Range(-4,1.65f), -0.5f), Quaternion.identity) as GameObject;
        itemList.Add(bottle);

        randomDis = Random.Range(50, 250);
    }

    public void DestoryItem(GameObject obj)
    {

        Destroy(obj);
        itemList.Remove(obj);
        
    }
    
}
