﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    // 整体速度控制
    private float speed = 0;
    // 基础整体速度
    private float baseSpeed = 0.8f;
    // 加至满速度时间 (秒)
    private float speedUpTime = 60f;
    // 当前时间 (秒)
    private float curTime = 0;
    // 当前距离
    private float distance = 0;

    // 增加的速度
    private float speedUp = 0;
    // 加速协程
    Coroutine speedUpCor;

    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        speed = baseSpeed;
    }

    void Update()
    {
        // 当前时间小于满速时间 && 玩家前进 累加当前时间
        Run();
    }

    private void Run()
    {
        if (curTime < speedUpTime && PlayerController.instance.CanMove)
        {
            curTime += Time.deltaTime;
            speed = speedUp + baseSpeed + baseSpeed * curTime / speedUpTime;
        }
    }

    // 更新距离UI
    public void UpdateDis(float dis)
    {
        MainView.instance.UpdateDis(dis);
        distance = dis;
    }
    // 获得距离
    public float GetDistance()
    {
        return distance;
    }

    // 移动位置
    public Vector3 MovePos()
    {
        return new Vector3(-ScrollCircle.instance.dir.x * 0.1f * GameManager.instance.Speed, 0, 0);
    }

    /// <summary>
    /// 加速
    /// </summary>
    /// <param name="time">时间(秒)</param>
    /// <param name="effect">增加的速度</param>
    public void SpeedUp(float time, float effect)
    {
        speedUp = effect;

        if(speedUpCor != null)
        {
            
            StopCoroutine(speedUpCor);
            
            speedUpCor = StartCoroutine(OnSpeedUp(time));
        }
        else
        {
            speedUpCor = StartCoroutine(OnSpeedUp(time));
        }
    }

    IEnumerator OnSpeedUp(float time)
    {
        yield return new WaitForSeconds(time);
        speedUp = 0;
        
    }

}
